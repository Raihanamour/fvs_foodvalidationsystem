---------------------------------FOOD VALIDATION SYSTEM----------------------------------

**UI User Guide**

1. Open http://localhost:3000 in the chrome browser
2. Navigates to the Homepage
3. Open http://localhost:3000/admin

4. Add Raw material dealer
   eg:
   companyID: 1001 // id of rawmaterail dealer
   companyName: Fissai // authorized delaer name
   email:fissai@gmail.com
   phone:987456
   companyAddress:Mavoor
   state:Kerala
   place:Calicut
   pincode:1236
   licenceNo:MNB123// licence number

5. View Raw material Dealer
   eg:
   companyID: 1001 // display details of raw material dealer

6. Add Manufacturer
   eg:
   companyID:123 // id of manufacturer
   companyName: Nestle // Manufacturer name
   email: nestle @gmail.com
   phone:4569872
   companyAddress:chembavoor // address of Manufacturer
   state:Kerala
   place:Palakkad
   pincode:1235
   licenceNo:ERT345 // licence number of Manufacturer

7. view Manufacturer
   eg:
   companyID: 123 // display details of Manufacturer

8. Add seller
   eg:
   sellerID: 321 // id of Seller
   sellerName: Bigfresh // name of seller who sale the product
   email: bigfresh@gmail.com
   phone:45698789
   sellerAddress: maathar // addreee of seller
   state: Kerala
   place: Kannur
   pincode: 46987
   licenceNo:BNM456

9. view seller
   eg:
   sellerId: 321 // display details of Seller

10. Add Raw material(by Raw material dealer)
    eg:
    batch: 1001 //raw materials are added to a batch
    materialID: 101 // unique identifier for raw materials
    materialName: Wheat
    quantity:100 // total quandity of each material
    numberOfProducts:10 // maximum count of product that can be produced by manufacture in a specified batch
    productName: Noodles // name of the product
    companyName: Nestle // name of manufacture
    validtill: 6 //expiry in months

11. Add Product(by Manufacturer)
    eg:
    productID:001 // unique identifier for a product
    productName: Noodles
    quantity: 10 // quandity of each product
    companyName: Nestle // manufacture name
    MRP:15 // price of each product
    expireInMonths: 6 // validity in months
    batch: 1001 // batch for the material

12. set bill(by Seller)
    eg:
    billID: 9001 // unique id for each bill
    shopID: 321 // seller id
    productID: 001 // id of product to be billed

13. USER
    User can view details of product by productID
    eg:
    productID: 001 // displays the details of product
